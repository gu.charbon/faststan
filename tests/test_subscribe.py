import pytest


@pytest.mark.asyncio
async def test_error_callback(app_and_client, capsys):
    app, client = app_and_client
    await app.stan.publish_json("dummy-model", {})
    captured = capsys.readouterr()
    assert "1 validation error for DummyModel" in captured.out


@pytest.mark.asyncio
async def test_pydantic_model_subscription(dummy_model, app_and_client, capsys):
    app, client = app_and_client
    await app.stan.publish_pydantic_model("dummy-model", dummy_model(foo="bar"))
    captured = capsys.readouterr()
    assert "bar\n" == captured.out


@pytest.mark.asyncio
async def test_constant(app_and_client, capsys):
    app, client = app_and_client
    await app.stan.publish("constant", b"hello")
    captured = capsys.readouterr()
    assert captured.out.count("always the same") == 2


@pytest.mark.asyncio
@pytest.mark.parametrize("channel", ["bytes-1", "bytes-2", "async"])
async def test_simplest_subscription(channel, app_and_client, capsys):
    app, client = app_and_client
    await app.stan.publish_str(channel, "Hello")
    captured = capsys.readouterr()
    assert captured.out == "b'Hello'\n"


@pytest.mark.asyncio
async def test_string_subscription(app_and_client, capsys):
    app, client = app_and_client
    await app.stan.publish_str("string-1", "Hello")
    captured = capsys.readouterr()
    assert captured.out == "Hello\n"


@pytest.mark.asyncio
@pytest.mark.parametrize("channel", ["list-1", "list-2"])
@pytest.mark.parametrize("content", [[1, 2, 3], ["a", "b", "c"]])
async def test_list_subscription(channel, content, app_and_client, capsys):
    app, client = app_and_client
    await app.stan.publish_json(channel, [1, 2, 3])
    captured = capsys.readouterr()
    assert captured.out == "3\n"


@pytest.mark.asyncio
async def test_float_list_subscription(app_and_client, capsys):
    app, client = app_and_client
    await app.stan.publish_json("list-3", [1, 2, 3])
    captured = capsys.readouterr()
    assert captured.out == "6.0\n"


@pytest.mark.asyncio
async def test_int_dict_subscription(app_and_client, capsys):
    app, client = app_and_client
    await app.stan.publish_json("dict-1", {"a": 1, "b": 2, "c": 3})
    captured = capsys.readouterr()
    assert captured.out == "6\n"


@pytest.mark.asyncio
async def test_float_dict_subscription(app_and_client, capsys):
    app, client = app_and_client
    await app.stan.publish_json("dict-2", {"a": 1, "b": 2, "c": 3})
    captured = capsys.readouterr()
    assert captured.out == "6.0\n"


@pytest.mark.asyncio
async def test_custom_parser_subscription(app_and_client, capsys):
    app, client = app_and_client
    await app.stan.publish_str("custom-1", "hello")
    captured = capsys.readouterr()
    assert captured.out == "hello\n"


@pytest.mark.asyncio
@pytest.mark.parametrize("channel", ["list-1", "list-2"])
@pytest.mark.parametrize("content", ["str", {"a": "b"}, {}, b"bytes"])
async def test_bad_list_subscription(channel, content, app_and_client, capsys):
    app, client = app_and_client
    try:
        await app.stan.publish_json(channel, content)
    except Exception:
        try:
            await app.stan.publish_str(channel, content)
        except Exception:
            await app.stan.publish(channel, bytes(content))
    captured = capsys.readouterr()
    assert "ERROR" in captured.out
