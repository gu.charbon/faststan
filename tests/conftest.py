from typing import List, Dict
import pytest
from fastapi.testclient import TestClient
from faststan import FastSTAN
from pydantic import BaseModel


@pytest.fixture
def dummy_model():
    class DummyModel(BaseModel):
        foo: str

    return DummyModel


@pytest.fixture
def dummy_error_cb():
    return lambda x: print(x)


@pytest.fixture
def app(dummy_model, dummy_error_cb):
    app_ = FastSTAN(client="pytest-client")

    @app_.stan.subscribe("constant")
    async def constant_subscription():
        print("always the same")

    @app_.stan.subscribe("constant")
    async def constant_subscription2():
        print("always the same")

    @app_.stan.subscribe("dummy-model", error_cb=dummy_error_cb)
    async def model_subscription(msg: dummy_model):
        print(msg.foo)

    @app_.stan.subscribe("async")
    async def async_subscription(msg):
        print(msg)

    @app_.stan.subscribe("bytes-1")
    def bytes_subscription(msg):
        print(msg)

    @app_.stan.subscribe("bytes-2")
    def bytes_subscription2(msg: bytes):
        print(msg)

    @app_.stan.subscribe("string-1")
    def string_subscription(msg: str):
        print(msg)

    @app_.stan.subscribe("list-1")
    def list_subscription(msg: list):
        print(len(msg))

    @app_.stan.subscribe("list-2")
    def list_subscription2(msg: List[str]):
        print(len(msg))

    @app_.stan.subscribe("list-3")
    def list_subscription3(msg: List[float]):
        print(sum(msg))

    def custom_parser(msg):
        return msg.decode()

    @app_.stan.subscribe("custom-1")
    def custom_parser_subscription(msg: custom_parser):
        print(msg)

    @app_.stan.subscribe("dict-1")
    def dict_subscription(msg: dict):
        print(sum(msg.values()))

    @app_.stan.subscribe("dict-2")
    def dict_subscription2(msg: Dict[str, float]):
        print(sum(msg.values()))

    return app_


@pytest.fixture
def app_and_client(app):
    with TestClient(app) as client:
        yield app, client
