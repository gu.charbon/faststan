def test_healthz(app_and_client):
    app, client = app_and_client
    response = client.get("/healthz")
    assert response.status_code == 200
    assert response.json() == {"status": "ok", "nats": "ok", "stan": "ok"}
