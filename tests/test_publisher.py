import pytest
from faststan.publisher import STANPublisher


@pytest.mark.asyncio
async def test_publisher():
    pub = STANPublisher()
    await pub.connect()
    await pub.publish("test", b"test")
