"""
Development tasks for FastSTAN project
"""
import contextlib
import os
from pathlib import Path
from shutil import rmtree

from invoke import task


PROJECT_DIR = Path(__file__).parent
SRC_DIR = PROJECT_DIR / "src"
TESTS_DIR = PROJECT_DIR / "tests"


SRC_FILES = " ".join([str(path) for path in SRC_DIR.glob("**/*.py")])
TEST_FILES = " ".join([str(path) for path in TESTS_DIR.glob("**/*.py")])


@contextlib.contextmanager
def chdir(dirname: str = None):
    """A context manager to temporary change directory."""
    # TODO: Add tests
    current_dir = Path.cwd()
    if Path(dirname).is_dir():
        os.chdir(dirname)
        yield
    else:
        raise FileNotFoundError("Directory {dirname} does not exist.")
    os.chdir(current_dir)


@task
def example(c, name="faststan", host="127.0.0.1", port="4222"):
    """Run an example."""
    os.environ["STAN_HOST"] = host
    os.environ["STAN_PORT"] = port
    os.environ["STAN_CLIENT"] = f"example-{name}"
    with chdir(f"./docs/examples/{name}"):
        if name == "faststan":
            c.run("uvicorn app:app")
        else:
            c.run("python app.py")


@task
def docs(c):
    """Serve the documentation."""
    c.run("mkdocs serve")


@task
def lint(c):
    """Lint the code sources."""
    c.run(f"flake8 {SRC_FILES} {TEST_FILES} tasks.py")


@task
def format(c):
    """Format the code sources."""
    c.run(f"black {SRC_FILES} {TEST_FILES} tasks.py")


@task
def test(c):
    """Run the unit tests."""
    c.run("pytest")


@task
def build(c, package=True, docs=True):
    """Build the package and optionally documentation."""
    if docs:
        c.run(f"mkdocs build -d {PROJECT_DIR / 'dist' / 'docs'}")
    if package:
        c.run("poetry build")


@task
def clean(c):
    """Clean the build artefacts."""

    def try_rm(path: Path):
        try:
            rmtree(path)
        except NotADirectoryError:
            path.unlink(missing_ok=True)
        except FileNotFoundError:
            pass

    try_rm(PROJECT_DIR / "dist")
    try_rm(PROJECT_DIR / "cov.xml")
    try_rm(PROJECT_DIR / "htmlcov")
    try_rm(PROJECT_DIR / ".coverage")
    [rmtree(directory) for directory in Path("src").glob("**/*.egg-info")]
    [rmtree(directory) for directory in Path("src").glob("**/__pycache__")]
    [rmtree(directory) for directory in Path("tests").glob("**/__pycache__")]


@task
def nb_kernel(c, name="captain-extractor", display_name="Captain Extractor"):
    """Register a jupyter kernel for current python interpreter."""
    c.run("python -m pip install ipykernel")
    c.run(
        f'python -m ipykernel install --user --name="{name}" --display-name="{display_name}"'
    )


@task
def build_ci_image(
    c,
    repository="gcharbon",
    name="faststan",
    tag="ci",
    cache_from="gcharbon/faststan:ci",
    cache_dir="/poetry",
):
    c.run(
        f"docker build \
            --cache-from {cache_from} \
            -t {repository}/{name}:{tag} \
            --build-arg CACHE_DIR={cache_dir} \
            -f ci/Dockerfile \
            ."
    )


@task
def push_ci_image(c, repository="gcharbon", name="faststan", tag="ci"):
    c.run(f"docker push {repository}/{name}:{tag}")
