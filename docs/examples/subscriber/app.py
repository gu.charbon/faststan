"""Minimal example of a FastSTAN application."""
from pydantic import BaseModel
from faststan.subscriber import STANSubscriber


class Event(BaseModel):
    timestamp: int
    temperature: float
    humidity: float


app = STANSubscriber()


def handle_error(error):
    print(f"ERROR: {error}")


@app.subscribe("event", error_cb=handle_error)
def on_event(event: Event):
    msg = f"INFO: New event on timestamp: {event.timestamp}. Temperature: {event.temperature}. Humidity: {event.humidity}"
    print(msg)


if __name__ == "__main__":
    print("Starting subscription on topic 'event'")
    app.run_forever()
